# ansible-kube
Set-up and management of a kubenetes cluster

Role Framework for Digital Ocean server configuration with ansible playbooks

Run a playbook with:

    ansible-playbook plays/<play-file> -i hosts/<hosts-file>


# Create a playbook

The playbook contains all roles to be played by which servers. Use a relative path from the plays directory:
```bash
---
- hosts: all
  roles:
    - role: ../roles/test
- hosts: master
  roles:
    - role: ../roles/example
```

# create a new role

in the roles directory:

    ansible-galaxy init <role-name>

This creates some directories:

- files: contains regular files that need to be transferred to the hosts you are configuring for this role.
- handlers: handlers are lists of tasks referenced by a globally unique name, and are notified by notifiers.
- meta: contains files that establish role dependencies. You can list roles that must be applied before the current role can work correctly.
- templates: files that use variables to substitute information during creation in this directory.
- tasks: contains all of the tasks, can reference files and templates contained in their respective directories without using a path.
- vars: Variables for the roles can be specified in this directory and used in your configuration files.

# Resources

https://www.digitalocean.com/community/tutorials/how-to-create-a-kubernetes-1-11-cluster-using-kubeadm-on-ubuntu-18-04